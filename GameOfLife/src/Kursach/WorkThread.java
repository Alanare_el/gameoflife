package Kursach;

public class WorkThread extends Thread {

    GameOfLife gameOfLife;
    Boolean isStop=false;

    WorkThread(GameOfLife gol){
        gameOfLife=gol;
    }


    @Override
    public void run() {
        while (!isInterrupted()) {
            synchronized (gameOfLife){
                for (int x = 0; x < gameOfLife.LIFE_SIZE; x++) {
                    for (int y = 0; y < gameOfLife.LIFE_SIZE; y++) {
                        int count = gameOfLife.countNeighbors(x, y);
                        gameOfLife.nextGeneration[x][y] = gameOfLife.lifeGeneration[x][y];
                        gameOfLife.nextGeneration[x][y] = (count == 3) ? true : gameOfLife.nextGeneration[x][y];
                        gameOfLife.nextGeneration[x][y] = ((count < 2) || (count > 3)) ? false : gameOfLife.nextGeneration[x][y];
                    }
                }
                gameOfLife.canvasPanel.repaint();
                for (int x = 0; x < gameOfLife.LIFE_SIZE; x++) {
                    System.arraycopy(gameOfLife.nextGeneration[x], 0, gameOfLife.lifeGeneration[x], 0, gameOfLife.LIFE_SIZE);
                }
            }
            if (isStop){
                try{
                    synchronized (this){
                        wait();
                    }
                }catch(InterruptedException e){}
            }
            try{
                Thread.sleep(gameOfLife.showDelay);
            }catch (InterruptedException ex){

            }
        }
    }
}
