package Kursach;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.MouseInputAdapter;
import java.util.*;

public class GameOfLife extends JFrame {

    final String NAME_OF_GAME = "Conway's Game of Life";
    final int START_LOCATION = 130;
    final int LIFE_SIZE = 50;
    final int POINT_RADIUS = 10;
    final int FIELD_SIZE = LIFE_SIZE * POINT_RADIUS + 7;
    final int BTN_PANEL_HEIGHT = 58;
    boolean[][] lifeGeneration = new boolean[LIFE_SIZE][LIFE_SIZE];
    boolean[][] nextGeneration = new boolean[LIFE_SIZE][LIFE_SIZE];
    protected int showDelay = 200;
    protected Canvas canvasPanel;
    private Random random = new Random();
    private WorkThread threadWork=new WorkThread(this);
    private Boolean flag=false;
    private Thread pot;


    public static void main(String[] args) {
        new GameOfLife().go();
    }

    void go() {
        JFrame frame = new JFrame(NAME_OF_GAME);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(FIELD_SIZE, FIELD_SIZE + BTN_PANEL_HEIGHT);
        frame.setLocation(START_LOCATION, START_LOCATION);
        frame.setResizable(false);

        canvasPanel = new Canvas();
        canvasPanel.setBackground(Color.white);

        JButton fillButton = new JButton("Fill");
        fillButton.addActionListener(new FillButtonListener());

        JButton stepButton = new JButton("Step");
        stepButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                processOfLife();
                canvasPanel.repaint();
            }
        });

        String[] items={
                "Clear",
                "Fill",
                "Glider"
        };

        JComboBox fillingOptions=new JComboBox(items);

        fillingOptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            String select=(String)fillingOptions.getSelectedItem();
            if(select=="Clear"){
                for (int x = 0; x < LIFE_SIZE; x++) {
                    for (int y = 0; y < LIFE_SIZE; y++) {
                        lifeGeneration[x][y] = false;
                    }
                }
                canvasPanel.repaint();
            }
            if(select=="Fill"){
                for (int x = 0; x < LIFE_SIZE; x++) {
                    for (int y = 0; y < LIFE_SIZE; y++) {
                        lifeGeneration[x][y] = random.nextBoolean();
                    }
                }
                canvasPanel.repaint();
            }
            if(select=="Glider"){
                lifeGeneration[4][1]=true;
                lifeGeneration[5][2]=true;
                lifeGeneration[5][3]=true;
                lifeGeneration[4][3]=true;
                lifeGeneration[3][3]=true;
                canvasPanel.repaint();
            }
            }
        });

        frame.addMouseListener(new MouseInputAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
            if(e.getButton()==1){
                int x=e.getX();
                int y=e.getY();
                lifeGeneration[x/10][y/10-3]=true;
                canvasPanel.repaint();
            }
            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });


        String[] itemsSpeed={
                "Speed",
                "Briskly",
                "Moderately",
                "Slowly"
        };
        JComboBox speedBox=new JComboBox(itemsSpeed);

        speedBox.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String select=(String)speedBox.getSelectedItem();
                if(select=="Briskly"){
                    showDelay=100;
                }
                if(select=="Moderately"){
                   showDelay=300;
                }
                if(select=="Slowly"){
                    showDelay=750;
                }
            }
        });


        final JButton goButton = new JButton("Play");
        goButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if(!flag){
                    //goNextGeneration=true;
                    System.out.println("Allah");
                    pot=new Thread(threadWork);
                    pot.start();
                    flag=true;
                }
                else {
                    if (threadWork.isStop) {
                        threadWork.isStop = false;
                        synchronized (threadWork) {
                            threadWork.notify();
                        }
                    } else {
                        threadWork.isStop = true;
                    }
                }
                goButton.setText(threadWork.isStop? "Play" : "Stop");
            }
        });

            this.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    try {
                        synchronized (threadWork) {
                            threadWork.interrupt();
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    super.windowClosing(e);
                }
            });

        Color myColor = new Color(64, 102, 102);
        Color myColor1=new Color(224,223,123);

        JPanel btnPanel = new JPanel();
        btnPanel.setBackground(myColor);
        speedBox.setBackground(myColor1);
        fillButton.setBackground(myColor1);
        stepButton.setBackground(myColor1);
        goButton.setBackground(myColor1);
        fillingOptions.setBackground(myColor1);

        btnPanel.add(speedBox);
        btnPanel.add(fillButton);
        btnPanel.add(stepButton);
        btnPanel.add(goButton);
        btnPanel.add(fillingOptions);

        frame.getContentPane().add(BorderLayout.CENTER, canvasPanel);
        frame.getContentPane().add(BorderLayout.SOUTH, btnPanel);
        frame.setVisible(true);


    }

    // произвольно заполняем ячейки
    public class FillButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent ev) {
            for (int x = 0; x < LIFE_SIZE; x++) {
                for (int y = 0; y < LIFE_SIZE; y++) {
                    lifeGeneration[x][y] = random.nextBoolean();
                }
            }
            canvasPanel.repaint();
        }
    }

    // подсчет количества соседей
    int countNeighbors(int x, int y) {
        int count = 0;
        for (int dx = -1; dx < 2; dx++) {
            for (int dy = -1; dy < 2; dy++) {
                int nX = x + dx;
                int nY = y + dy;
                nX = (nX < 0) ? LIFE_SIZE - 1 : nX;
                nY = (nY < 0) ? LIFE_SIZE - 1 : nY;
                nX = (nX > LIFE_SIZE - 1) ? 0 : nX;
                nY = (nY > LIFE_SIZE - 1) ? 0 : nY;
                count += (lifeGeneration[nX][nY]) ? 1 : 0;
            }
        }
        if (lifeGeneration[x][y]) { count--; }
        return count;
    }

    // основной процесс жизни
    void processOfLife() {
        for (int x = 0; x < LIFE_SIZE; x++) {
            for (int y = 0; y < LIFE_SIZE; y++) {
                int count = countNeighbors(x, y);
                nextGeneration[x][y] = lifeGeneration[x][y];
                // если вокруг пустых ячеек находятся 3 живых соседа-ячейка становится живой
                nextGeneration[x][y] = (count == 3) ? true : nextGeneration[x][y];
                // если у клетки меньше 2 или больше 3 соседей-она умре
                nextGeneration[x][y] = ((count < 2) || (count > 3)) ? false : nextGeneration[x][y];
            }
        }
        for (int x = 0; x < LIFE_SIZE; x++) {
            System.arraycopy(nextGeneration[x], 0, lifeGeneration[x], 0, LIFE_SIZE);
        }
    }

    // paint on the canvas
    public class Canvas extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            for(int i=1;i<50;i++){
                g.drawLine(10*i,0,10*i,50*10);
            }
            for(int i=1;i<50;i++){
                g.drawLine(0,10*i,50*10,10*i);
            }
            for (int x = 0; x < LIFE_SIZE; x++) {
                for (int y = 0; y < LIFE_SIZE; y++) {
                    if (lifeGeneration[x][y]) {
                        g.fillOval(x*POINT_RADIUS, y*POINT_RADIUS, POINT_RADIUS, POINT_RADIUS);
                    }
                }
            }
        }
    }
}